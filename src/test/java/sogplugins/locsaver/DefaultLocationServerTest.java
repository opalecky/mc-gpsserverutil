package sogplugins.locsaver;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DefaultLocationServerTest {
    DefaultLocationServer dfls = new DefaultLocationServer();

    String TestPlayerName = "TestPlayer";
    String TestPlayerName2 = "TestPlayer2";
    LocationOnMap location0 = new LocationOnMap(TestPlayerName, "Loc0", new int[]{0, 0, 1}, false,0);
    LocationOnMap location1 = new LocationOnMap(TestPlayerName, "Loc1", new int[]{0, 1, 0}, false,0);
    LocationOnMap location2 = new LocationOnMap(TestPlayerName2, "Loc2", new int[]{1, 0, 0}, true,0);

    @org.junit.jupiter.api.BeforeEach
    void before() {
        DefaultLocationServer dfls = new DefaultLocationServer();
        location0 = new LocationOnMap(TestPlayerName, "Loc0", new int[]{0, 0, 1}, false,0);
        LocationOnMap location1 = new LocationOnMap(TestPlayerName, "Loc1", new int[]{0, 1, 0}, false,0);
        LocationOnMap location2 = new LocationOnMap(TestPlayerName2, "Loc2", new int[]{1, 0, 0}, true,0);
    }

    @org.junit.jupiter.api.AfterEach
    void after() {
        dfls.savedLocations.clear();
    }

    @org.junit.jupiter.api.Test
    void locationExistsOnInit() {
        assertEquals(0b00, dfls.locationExists(TestPlayerName, location1.m_locationName));
    }

    @org.junit.jupiter.api.Test
    void locationExistsAfterAddingASingleLocation() {
        dfls.locationDictionary.put(TestPlayerName, new ArrayList<>());
        dfls.savedLocations.add(location0);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location0.m_locationName, dfls.savedLocations.size() - 1));
        assertEquals(0b11, dfls.locationExists(TestPlayerName, location0.m_locationName));
    }

    @org.junit.jupiter.api.Test
    void locationExistsOnInitAfterAddingMultipleLocations() {
        dfls.locationDictionary.put(TestPlayerName, new ArrayList<>());
        dfls.savedLocations.add(location0);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location0.m_locationName, dfls.savedLocations.size() - 1));
        dfls.savedLocations.add(location1);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location1.m_locationName, dfls.savedLocations.size() - 1));
        dfls.savedLocations.add(location2);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location2.m_locationName, dfls.savedLocations.size() - 1));
        assertEquals(0b11, dfls.locationExists(location0.m_playerName, location0.m_locationName));
        assertEquals(0b11, dfls.locationExists(location1.m_playerName, location1.m_locationName));
        assertEquals(0b11, dfls.locationExists(location2.m_playerName, location2.m_locationName));
    }

    @org.junit.jupiter.api.Test
    void locationExistsOnInitAfterRemovingLocations() {
        dfls.locationDictionary.put(TestPlayerName, new ArrayList<>());
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location0.m_locationName, dfls.savedLocations.size()));
        dfls.savedLocations.add(location0);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location1.m_locationName, dfls.savedLocations.size()));
        location1.markForDeletion();
        dfls.savedLocations.add(location1);
        dfls.locationDictionary.get(TestPlayerName).add(new LocationPlacement(location2.m_locationName, dfls.savedLocations.size()));
        dfls.savedLocations.add(location2);
        assertEquals(0b11, dfls.locationExists(location0.m_playerName, location0.m_locationName));
        assertEquals(0b10, dfls.locationExists(location1.m_playerName, location1.m_locationName));
        assertEquals(0b11, dfls.locationExists(location2.m_playerName, location2.m_locationName));
        location1.m_markedForDeletion = false;
    }

    @org.junit.jupiter.api.Test
    void addLocationEdgeCaseNull() {
        assertEquals(ILocationServer.RETURN_CODES.ERROR, dfls.addLocation(null, true));
    }

    @org.junit.jupiter.api.Test
    void addOneValidLocation() {
        assertEquals(ILocationServer.RETURN_CODES.SUCCESS, dfls.addLocation(location0, true));
        assertEquals(0b11, dfls.locationExists(location0.m_playerName, location0.m_locationName));
    }

    @org.junit.jupiter.api.Test
    void addMultipleValidLocations() {
        assertEquals(ILocationServer.RETURN_CODES.SUCCESS, dfls.addLocation(location0, true));
        assertEquals(0b11, dfls.locationExists(TestPlayerName, location0.m_locationName));
        assertEquals(ILocationServer.RETURN_CODES.SUCCESS, dfls.addLocation(location1, true));
        assertEquals(0b11, dfls.locationExists(TestPlayerName, location1.m_locationName));
        assertEquals(ILocationServer.RETURN_CODES.SUCCESS, dfls.addLocation(location2, true));
        assertEquals(0b11, dfls.locationExists(TestPlayerName, location2.m_locationName));
    }

    @org.junit.jupiter.api.Test
    void getLocation() {
    }

    @org.junit.jupiter.api.Test
    void removeLocation() {
    }

    @org.junit.jupiter.api.Test
    void exportData() {

    }

    @org.junit.jupiter.api.Test
    void importData() {

    }
}