package sogplugins.locsaver;

import java.io.File;
import java.util.ArrayList;

public interface ILocationServer {
    /**
     * Describes different return states of functions
     * @see java.lang.Enum
     */
    public static enum RETURN_CODES {
        /**
         * Successful execution of command
         */
        SUCCESS,
        /**
         * Location exists success message
         */
        LOCATION_EXISTS,
        /**
         * Location is owned by another player
         * @accessibility -- mostly internal error --
         */
        LOCATION_NOT_OWNED,
        /**
         * Location with given name has been already saved by this specific player
         * @accessibility -- internal information to update location --
         */
        LOCATION_ALREADY_SAVED,
        /**
         * Permission test and error so that global locations can only be set and updated by OPs
         */
        LOCATION_ALREADY_SAVED_GLOBAL,
        /**
         * Unexpected unhandled exception happened.
         * @WhatIsHappening Code is freaking out.
         */
        ERROR
    }

    /**
     * All locations saved by all players are stored here with their specifics
     *
     * @see ArrayList
     * @see ISavedLocation
     * @type ArrayList<ISavedLocation>
     */
    public ArrayList<ISavedLocation> savedLocations = new ArrayList<ISavedLocation>();

    /**
     * Look into saved locations and determines whether a location is stored and not marked for deletion
     *
     * @param in_playerName Name of looking player
     * @param in_locationName Name of location they are looking for
     * @return Status of search
     */
    public int locationExists(String in_playerName, String in_locationName);

    /**
     * Add location to list of saved locations.
     *
     * @param in_location Location to store
     * @param op OP status of player who is trying to add location
     * @return Status of success
     */
    RETURN_CODES addLocation(ISavedLocation in_location, boolean op);

    /**
     * Retrieves location from SavedLocations list
     *
     * @param in_playerName Name of player
     * @param in_locationName Name of location
     * @return [x,y,z] coordinates in an array (in order)
     */
    int[] getLocation(String in_playerName, String in_locationName);

    /**
     * Flags location for removal in SavedLocations list
     *
     * @param in_playerName Name of player who wants to remove the location
     * @param in_locationName Name of location to remove
     * @param op OP status (for global locations)
     * @return Result of action
     */
    RETURN_CODES removeLocation(String in_playerName, String in_locationName, boolean op);

    /**
     * Exports data to a file in CSV format
     *
     * @param location Data folder of plugin
     * @param fileName Name of file to save to
     * @return Exported data
     */
    String exportData(File location, String fileName);

    /**
     * Imports data from previous iteration of server restart
     *
     * @param data Previous iteration data
     * @return success state
     */
    boolean importData(String data);
}
