package sogplugins.locsaver;

import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.logging.Logger;

class LocationPlacement {
    public String name;
    public int index;

    LocationPlacement(String in_name, int in_index) {
        name = in_name;
        index = in_index;
    }
}

public class DefaultLocationServer implements ILocationServer {
    Logger logger = Logger.getLogger("CommandKitLogger");
    public final HashMap<String, ArrayList<LocationPlacement>> locationDictionary = new HashMap<String, ArrayList<LocationPlacement>>();
    public final HashMap<String, LocationPlacement> globalLocationDictionary = new HashMap<String, LocationPlacement>();

    public int __getLocationIndex(ArrayList<LocationPlacement> in_list, String in_locationName) {
        int index = Integer.MAX_VALUE;
        for (LocationPlacement lp : in_list) if (lp.name.equals(in_locationName)) index = lp.index;
        return index;
    }

    private boolean __isGlobal(String locationName) {
        return globalLocationDictionary.get(locationName) != null;
    }

    @Override
    public int locationExists(String in_playerName, String in_locationName) {
        if (__isGlobal(in_locationName)) {
            return 0b11;
        }
        if (locationDictionary.isEmpty()) return 0b00;
        ArrayList<LocationPlacement> locations = locationDictionary.get(in_playerName);
        if (locations == null) return 0b01;
        int index = __getLocationIndex(locations, in_locationName);
        if (index == Integer.MAX_VALUE) return 0b10;
        LocationOnMap lom = (LocationOnMap) savedLocations.get(index);
        if (lom.m_markedForDeletion) return 0b10;
        return 0b11;
    }

    @Override
    public RETURN_CODES addLocation(ISavedLocation in_location, boolean op) {
        if (in_location == null) return RETURN_CODES.ERROR;
        LocationOnMap localLocation = (LocationOnMap) in_location;
        if (localLocation.m_isGlobal && !op)
            return RETURN_CODES.LOCATION_ALREADY_SAVED_GLOBAL;
        int locStatus = this.locationExists(localLocation.m_playerName, localLocation.m_locationName);
        switch (locStatus) {
            case 0b00:
            case 0b01:
            case 0b10:
                if (localLocation.m_isGlobal) {
                    globalLocationDictionary.put(localLocation.m_locationName, new LocationPlacement(localLocation.m_locationName, savedLocations.size()));
                    savedLocations.add(in_location);
                    return RETURN_CODES.SUCCESS;
                }
                if (locStatus != 0b10) {
                    locationDictionary.put(localLocation.m_playerName, new ArrayList<LocationPlacement>());
                }
                locationDictionary.get(localLocation.m_playerName).add(new LocationPlacement(localLocation.m_locationName, savedLocations.size()));
                savedLocations.add(in_location);
                return RETURN_CODES.SUCCESS;
            case 0b11:
                int index;
                if (localLocation.m_isGlobal || __isGlobal(localLocation.m_locationName)) {
                    if (!op || (!localLocation.m_isGlobal && __isGlobal(localLocation.m_locationName))) {
                        return RETURN_CODES.LOCATION_ALREADY_SAVED_GLOBAL;
                    }
                    index = globalLocationDictionary.get(localLocation.m_locationName).index;
                } else {
                    index = __getLocationIndex(locationDictionary.get(localLocation.m_playerName), localLocation.m_locationName);
                }
                savedLocations.set(index, localLocation);
                return RETURN_CODES.LOCATION_EXISTS;
        }
        return RETURN_CODES.ERROR;
    }

    @Override
    public int[] getLocation(String in_playerName, String in_locationName) {
        if (__isGlobal(in_locationName)) {
            LocationOnMap gloc = (LocationOnMap) savedLocations.get(globalLocationDictionary.get(in_locationName).index);
            return new int[]{gloc.m_x, gloc.m_y, gloc.m_z, gloc.m_worldId};
        }
        int index = __getLocationIndex(locationDictionary.get(in_playerName), in_locationName);
        LocationOnMap loc = (LocationOnMap) savedLocations.get(index);
        return new int[]{loc.m_x, loc.m_y, loc.m_z, loc.m_worldId};
    }

    @Override
    public RETURN_CODES removeLocation(String in_playerName, String in_locationName, boolean op) {
        if (__isGlobal(in_locationName) && !op) {
            return RETURN_CODES.LOCATION_NOT_OWNED;
        }
        int locationState = locationExists(in_playerName, in_locationName);
        if (locationState < 0b10) return RETURN_CODES.LOCATION_NOT_OWNED;
        int index = __getLocationIndex(locationDictionary.get(in_playerName), in_locationName);
        savedLocations.get(index).markForDeletion();
        return RETURN_CODES.SUCCESS;
    }

    @Override
    public String exportData(File location, String fileName) {
        if (!location.exists()) {
            try {
                if (location.mkdirs()) {
                    logger.info("Created folder at " + location.getAbsolutePath());
                } else {
                    logger.warning("Couldn't create folder at " + location.getAbsolutePath());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        logger.info(location);
        StringBuilder sb = new StringBuilder();
        for (ISavedLocation _loc : savedLocations) {
            LocationOnMap loc = (LocationOnMap) _loc;
            if (loc.m_markedForDeletion) continue;
            sb.append(loc.m_playerName).append(",")
                    .append(loc.m_locationName).append(",")
                    .append(loc.m_x).append(",")
                    .append(loc.m_y).append(",")
                    .append(loc.m_z).append(",")
                    .append(loc.m_isGlobal ? "1" : "0").append(",")
                    .append(loc.m_worldId).append("\n");
        }

        String finalString = sb.toString();
        File file = new File(location.getAbsolutePath(), fileName);
        try {
            FileWriter fw = new FileWriter(file);
            fw.write(finalString);
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalString;
    }

    @Override
    public boolean importData(String data) {
        for (String line : data.split("\n")) {
            logger.info(line);
            if (Objects.equals(line, "")) continue;
            String[] values = line.split(",");
            String player = values[0];
            String locationName = values[1];
            int x = Integer.parseInt(values[2]);
            int y = Integer.parseInt(values[3]);
            int z = Integer.parseInt(values[4]);
            boolean global = values[5].equals("1");
            int worldId = values.length < 7 ? 0 : Integer.parseInt(values[6]);
            if (global) logger.info(x + "\t" + y + "\t" + z);
            LocationOnMap lom = new LocationOnMap(player, locationName, new int[]{x, y, z}, global, worldId);
            addLocation(lom, true);
        }
        return true;
    }

    public ArrayList<String> getPlayerAvailableLocations(String playerName) {
        ArrayList<String> all = new ArrayList<String>();
        try {

            ArrayList<String> globals = new ArrayList<String>(globalLocationDictionary.keySet());
            ArrayList<String> personal = new ArrayList<String>();
            for (LocationPlacement lp : locationDictionary.get(playerName)) personal.add(lp.name);

            all.addAll(globals);
            all.addAll(personal);
            return all;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
