package sogplugins.locsaver;

public class LocationOnMap implements ISavedLocation{
    int m_x, m_y, m_z;
    boolean m_isGlobal;
    boolean m_markedForDeletion;
    String m_playerName;
    String m_locationName;
    int m_worldId;

    LocationOnMap(String in_playerName, String in_locationName, int[] location, boolean in_isGlobal, int in_worldId) {
        m_x = location[0];
        m_y = location[1];
        m_z = location[2];
        m_worldId = in_worldId;
        m_isGlobal = in_isGlobal;
        m_locationName = in_locationName;
        m_playerName = in_playerName;
    }
    @Override
    public boolean updateGlobality(boolean new_globality) {
        m_isGlobal = new_globality;
        return true;
    }

    @Override
    public void markForDeletion() {
        m_markedForDeletion = true;
    }
}
