package sogplugins.locsaver;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;
import java.util.logging.Logger;

public final class Locsaver extends JavaPlugin {
    LocSaverCommandKit kit = new LocSaverCommandKit();
    Logger logger = Logger.getLogger("MainLogger");

    @Override
    public void onEnable() {
        logger.info("================================");
        logger.info("Starting SoG::LocSaver now!");
        File savedLocations = new File(getDataFolder(), "saved_locations.csv");
        try {
            try (FileReader fr = new FileReader(savedLocations.getPath())) {
                BufferedReader br = new BufferedReader(fr);
                StringBuilder allLines = new StringBuilder();
                String line = br.readLine();
                do {
                    allLines.append(line).append("\n");
                    line = br.readLine();
                } while(line != null);
                logger.info("------------\n-------------\n------------"+allLines+"------------\n-------------\n------------");
                kit.m_locationServer.importData(allLines.toString());
            }
        } catch (FileNotFoundException e) {
            if(!savedLocations.mkdirs()) logger.warning("Couldn't create plugin folders!!");
            else logger.info("Plugin folders created successfully!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(this.getCommand("gps")).setExecutor(kit);
        logger.info("================================");
    }
    @Override
    public void onDisable() {
        logger.info("================================");
        logger.info("Exporting saved locations");
        logger.info(kit.m_locationServer.exportData(getDataFolder(), "saved_locations.csv"));
        logger.info("Stopping SoG::LocSaver now!");
        logger.info("================================");
    }
}
