package sogplugins.locsaver;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

import static org.bukkit.Bukkit.*;

public class LocSaverCommandKit implements CommandExecutor {

    Logger logger = Logger.getLogger("CommandKitLogger");

    public DefaultLocationServer m_locationServer = new DefaultLocationServer();

    private boolean __saveLocation(Player player, String in_locationName, int[] in_playerLocation, boolean in_isGlobal, int in_worldId, boolean op) {
        switch (m_locationServer.addLocation(new LocationOnMap(player.getName().toLowerCase(), in_locationName, in_playerLocation, in_isGlobal, in_worldId), op)) {
            case SUCCESS:
                return true;
            case LOCATION_ALREADY_SAVED:
                player.sendMessage("Couldn't save location because it already exists!");
                return false;
            case LOCATION_ALREADY_SAVED_GLOBAL:
                player.sendMessage("Couldn't save location because it already exists as a global location! Please pick another name!");
                return false;
            case LOCATION_NOT_OWNED:
                player.sendMessage("Cannot save this location because you do not own it!");
                return false;
            case LOCATION_EXISTS:
                player.sendMessage("Updated location!");
                return true;
            case ERROR:
                player.sendMessage("An error occurred while saving location!");
                return false;
        }
        return false;
    }

    private int[] __findLocation(String in_playerName, String in_locationName) {
        // find location
        if (__locationExists(in_playerName.toLowerCase(), in_locationName))
            return new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE};
        return m_locationServer.getLocation(in_playerName.toLowerCase(), in_locationName);
    }

    private boolean __removeLocation(String in_playerName, String in_locationName, boolean op) {
        return m_locationServer.removeLocation(in_playerName.toLowerCase(), in_locationName, op) == ILocationServer.RETURN_CODES.SUCCESS;
    }

    private boolean __locationExists(String in_playerName, String in_locationName) {
        return m_locationServer.locationExists(in_playerName.toLowerCase(), in_locationName) != 0b11;
    }

    private boolean __locationIsGlobal(String in_locationName) {
        return m_locationServer.globalLocationDictionary.get(in_locationName) != null;
    }

    private boolean __referToHelp(Player player) {
        player.sendMessage("Cannot perform \"/loc\" command. Please use /loc help to see proper argument specification.");
        // command is actually successful because we caught the issue
        return true;
    }

    private String __getWorldName(int id) {
        // start with overworld name
        String worldName = getWorlds().get(0).getName();

        logger.info(worldName);

        switch (id) {
            case 1:
                // add nether
                worldName += "_nether";
                return worldName;
            case 2:
                // add the_end
                worldName += "_the_end";
                return worldName;
            default:
                // fall through
                // || for now ||
                break;
        }
        return worldName;
    }

    private static final String __helpMessage = "Write '/gps <save | find | delete> [name of your location]' to save it!\n    " + "use Save to create a new named location. Use this also to update an existing location\n    " + "use Find to see coordinates of already created location\n    " + "use tp to teleport to home or a global location\n    " + "use Delete to remove a location that you no longer need\n" + "Name of your location can be any name composed of ASCII charset!";

    private boolean __saveCommand(Player player, String locationName, boolean isGlobal) {
        int worldId = 0; // save in overworld by default
        Location pLoc = player.getLocation();
        // find which world player is in
        switch (player.getWorld().getEnvironment()) {
            case NETHER:
                worldId = 1;
                break;
            case THE_END:
                worldId = 2;
                break;
        }

        // try to save location
        if (__saveLocation(player, locationName, new int[]{pLoc.getBlockX(), pLoc.getBlockY(), pLoc.getBlockZ()}, isGlobal, worldId, player.isOp()))
            player.sendMessage("Saved name " + locationName + " for location [x:" + pLoc.getBlockX() + ", y: " + pLoc.getBlockY() + ", z: " + pLoc.getBlockZ() + "]");
        else player.sendMessage("Failed to save location: " + locationName);
        return true;
    }

    public boolean __findCommand(Player player, String locationName) {
        int[] loc = __findLocation(player.getName(), locationName);
        if (Arrays.stream((new int[]{loc[0], loc[1], loc[2]})).anyMatch((e) -> e == Integer.MAX_VALUE)) {
            // this location was not found
            player.sendMessage("This location is not defined!");
            return true;
        }
        String message = locationName + " is at [x: " + loc[0] + ", y: " + loc[1] + ", z: " + loc[2] + ", " + loc[3] + "]";
        if (!player.getWorld().getName().equalsIgnoreCase(__getWorldName(loc[3]))) player.sendMessage(message);
        Location pLoc = player.getLocation();
        int deltaX = loc[0] - pLoc.getBlockX();
        int deltaZ = loc[2] - pLoc.getBlockZ();

        double distance = Math.sqrt(deltaX * deltaX + deltaZ * deltaZ);

        message += " that is " + Math.round(distance) + " blocks away in ";

        String CardinalDirection = "";
        // W: -X // E: +X // N: -Z // S: +Z

        int absX = Math.abs(deltaX);
        int absZ = Math.abs(deltaZ);

        if (absZ > 0.5 * absX) {
            // it makes sense to show N/S cardinal direction

            if (deltaZ < 0) {
                CardinalDirection += "N";
            } else if (deltaZ > 0) {
                CardinalDirection += "S";
            }
        }
        if (absX > 0.5 * absZ) {
            // it makes sense to show E/W cardinal direction
            if (deltaX > 0) {
                CardinalDirection += "E";
            } else if (deltaX < 0) {
                CardinalDirection += "W";
            }
        }

        message += CardinalDirection + " direction!";

        player.sendMessage(message);
        return true;
    }

    public boolean __removeLocation(Player player, String locationName) {
        if (__locationExists(player.getName(), locationName)) {
            // location user is trying to delete is not theirs or doesn't exist.
            player.sendMessage("Couldn't find given location!");
            return true;
        }
        if (!__removeLocation(player.getName(), locationName, player.isOp())) {
            // player doesn't have rights to remove this location!
            player.sendMessage("Failed to delete location " + locationName + "!");
            return true;
        }

        player.sendMessage("Successfully deleted location " + locationName + "!");
        return true;
    }

    private boolean __teleportCommand(Player player, String locationName) {
        if (__locationExists(player.getName(), locationName)) {
            // location doesn't exist or player is not an owner and/or it is not global
            player.sendMessage("Location " + locationName + " cannot be teleported to!");
            return true;
        }

        if (!locationName.equals("home") && !__locationIsGlobal(locationName) && !player.isOp()) {
            // player doesn't have right to teleport to specified location
            player.sendMessage("Location " + locationName + " does not bear the name home nor is a global location!");
            return true;
        }

        //  0 - X
        //  1 - Y
        //  2 - Z
        //  4 - World ID (0 = overworld, 1 = nether, 2 = end)
        int[] locationBlocks = m_locationServer.getLocation(player.getName().toLowerCase(), locationName);
        String worldName = __getWorldName(locationBlocks[3]);

        // console log for OP to see that someone teleported
        logger.info("Teleporting " + player.getName() + " to " + locationName + " at " + worldName + " [" + locationBlocks[0] + "," + locationBlocks[1] + "," + locationBlocks[2] + "]");
        if (player.teleport(new Location(locationBlocks[3] == 0 ? getWorlds().get(0) : getWorld(worldName), locationBlocks[0], locationBlocks[1], locationBlocks[2]))) {
            player.sendMessage("Teleported to location " + locationName);
            return true;
        }
        // teleport failed for some reason. idk... You tell me... FFS I can't even imagine why this would occur, and it's not my problem....
        player.sendMessage("Failed to teleport to location " + locationName);
        return true;
    }

    private boolean __listCommand(Player player) {
        try {
            ArrayList<String> locs = m_locationServer.getPlayerAvailableLocations(player.getName().toLowerCase());
            StringBuilder allAvailableLocs = new StringBuilder();
            allAvailableLocs.append("You have ").append(locs.size()).append(" available locations: ");
            int iter = 0;
            for (String l : locs) {
                allAvailableLocs.append(m_locationServer.globalLocationDictionary.get(l) != null ? "§6" : "§F");
                allAvailableLocs.append(l);
                if (++iter != locs.size()) allAvailableLocs.append(", ");
            }
            player.sendMessage(allAvailableLocs.toString());
            return true;
        }
        catch (Exception e) {
            player.sendMessage("Couldn't retrieve list of your locations!");
        }
        return false;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command cmd, @NotNull String label, String[] args) {
        // player requests help
        if (args.length == 1 && args[0].equalsIgnoreCase("help")) {
            sender.sendMessage(__helpMessage);
            return true;
        }

        // player put too few arguments and help was not one of them
        if (args.length < 2 && !args[0].equalsIgnoreCase("list")) return this.__referToHelp((Player) sender);
        else if(args[0].equalsIgnoreCase("list")) return __listCommand((Player) sender);
        try {
            Player player = (Player) sender;
            String commandName = args[0].toLowerCase();
            String locationName = args[1].toLowerCase();
            boolean isGlobal = false;
            boolean isTpLocation = false;
            if (args.length == 3) {
                // global locations are only assignable by OP.
                isGlobal = args[2].equalsIgnoreCase("global");

                // Will be used to assign location as teleportable when common players get their
                // amount of teleportable locations ready
                isTpLocation = args[2].equalsIgnoreCase("tp");
            }

            switch (commandName) {
                case "save":
                    return __saveCommand(player, locationName, isGlobal);
                case "find":
                    return __findCommand(player, locationName);
                case "delete":
                    return __removeLocation(player, locationName);
                case "tp":
                    return __teleportCommand(player, locationName);
                case "list":
                    return __listCommand(player);
                default:
                    player.sendMessage("Command does not contain proper action. Available actions are: Save, Find, Delete | Help");
                    // fall through
            }

        } catch (ClassCastException e) {
            logger.warning("Couldn't parse Sender to Player class!");
        } catch (Error e) {
            logger.warning("Unexpected exception while calling: " + cmd);
            e.printStackTrace();
        }
        return false;
    }

}
