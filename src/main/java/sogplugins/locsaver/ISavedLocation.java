package sogplugins.locsaver;

public interface ISavedLocation {
    /**
     * Position in Gaussian Space (block level precision)
     */
    int m_x = 0, m_y = 0, m_z = 0;

    /**
     * Player who stored it
     */
    String m_playerName = "";
    /**
     * Namer of stored location
     */
    String m_locationName = "";
    /**
     * Globality of location
     */
    boolean m_isGlobal = false;
    /**
     * Status of location activity
     */
    boolean m_markedForDeletion = false;

    int m_worldId = 0;


    /**
     * Change state of globality on location
     *
     * @param new_globality New state
     * @return Success (always true)
     */
    boolean updateGlobality(boolean new_globality);

    /**
     * Flag location for deletion.
     */
    void markForDeletion();
}
